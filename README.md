# PKGBUILDS for Arch based Systems 

[![Maintenance](https://img.shields.io/maintenance/yes/2023.svg)]()

PKGBUILDS **of interest** to Arch based Systems.<br>
The source for packages I maintain in my repository.

05-24-2023 main upload PKGBUILD

--- 

- **alma-aur** - **A**rch **L**inux **M**obile **A**ppliance

---
