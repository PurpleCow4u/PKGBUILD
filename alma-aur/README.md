# alma-aur

## About
alma-aur is a PKGBUILD for ALMA.

## Which Package? 
**ALMA** - **A**rch **L**inux **M**obile **A**ppliance
ALMA is a persistent live system for flash drives (USB,.img), based on Arch-Linux.


## Building the Package
### Requirements for Building the Package
* Is required: *git* and *rust* (`sudo pacman -S git rust`) 

### Cloning the Repository
~~~ csh
$ git clone https://codeberg.org/PurpleCow4u/PKGBUILD.git
~~~

### Building the Packages with *makepkg*
#### Preparation
Change directory to alma-aur and run *makepkg`
- -s = sync
- -i = install automatic after build
- -L = with output logfile

~~~ csh
$ cd alma-aur
$ makepkg -siL
~~~

#### Building
By default, the build script builds a 64-bit (x86_64) package.
For a aarch64 package set `ARCH=aarch64` or `ARCH=any` accordingly in `PGKBUILD`.

### Current Version
--- 

05-24-2023 version: **alma-aur-r139.36136c3-1-x86_64.pkg.tar.zst**
- with alma version 0.11.0

---
